﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using ImportingExelToDB.Models;

namespace ImportingExelToDB.DbContexts
{
    internal class ImportingDbContext : DbContext
    {
        
        

            public DbSet<Address> AddresDbSet { get; set; }
            public DbSet<Agreement> AgreementDbSet { get; set; }
            public DbSet<FinancialState> FinancialDbSet { get; set; }
            public DbSet<Person> PersonDbSet { get; set; }

        public ImportingDbContext()
            : base("ExcelDb")
        {
        }
        

    }
}
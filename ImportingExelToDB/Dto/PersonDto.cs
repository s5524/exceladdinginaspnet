﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ImportingExelToDB.Models;

namespace ImportingExelToDB.Dto
{
    public class PersonDto
    {
        public int Id;
        public string FirsName;
        public string SecondName;
        public string Surname;
        public string NationalIdentificationNumber;
        public Address Addresid;
        public string Phonenumber;
        public string Phonenumber2;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImportingExelToDB.Dto
{
    public class AddressDto
    {
        public int Id;
        public string StreetName;
        public string StreetNumber;
        public string FlatNumber;
        public string PostCode;
        public string PostOfficeCity;
        public string CorrespondenceStreetName;
        public string CorrespondenceStreetnumber;
        public string CorrespondenceFlatNumber;
        public string CorrespondencePostCode;
        public string CorrespondencePostOfficeCity;
    }
}
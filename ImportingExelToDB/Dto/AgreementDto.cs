﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ImportingExelToDB.Models;

namespace ImportingExelToDB.Dto
{
    public class AgreementDto
    {
        public int Id;
        public string Number;
        public Person Personid;
        public FinancialState FinancialStateId;
    }
}
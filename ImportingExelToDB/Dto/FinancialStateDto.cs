﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ImportingExelToDB.Dto
{
    public class FinancialStateDto
    {
        public int Id;
        public decimal OutstandingLiabilites;
        public decimal Interests;
        public decimal PenaltyInterests;
        public decimal Fees;
        public decimal CourtFees;
        public decimal RepresentationCourtFees;
        public decimal VindicationCosts;
        public decimal RepresentationVindicationCosts;
    }
}
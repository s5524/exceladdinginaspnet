﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using ExcelDataReader;
using ImportingExelToDB.DbContexts;
using ImportingExelToDB.Dto;
using ImportingExelToDB.Models;
using ImportingExelToDB.Mappers;
using ImportingExelToDB.Repository;
using ImportingExelToDB.ServiceReference1;
using Microsoft.Ajax.Utilities;

namespace ImportingExelToDB.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //var p = new Test();
            return View();
        }
        [Route("Add")]
        [HttpPost]
        public ActionResult Add(HttpPostedFileBase file)
        {
            IExcelDataReader reader = null;
            string fileExtension = Path.GetExtension(Request.Files["file"].FileName);

            if (file != null && file.ContentLength > 0)
            {
                if (fileExtension == ".xlsx")
                {
                    reader = ExcelReaderFactory.CreateOpenXmlReader(file.InputStream);
                }
                else if (fileExtension == ".xls")
                {
                    reader = ExcelReaderFactory.CreateBinaryReader(file.InputStream);
                }
                else
                {
                    return Content("Bed data");
                }
            }
            else
            {
                return View();
            }
            var addressDto = new AddressDto();
            var agreementDto = new AgreementDto();
            var financialStateDto = new FinancialStateDto();
            var personDto = new PersonDto();
            var excelTable = reader.AsDataSet().Tables[0];
            var repo = new ExcelRepository();

            for (int i = 1; i < excelTable.Rows.Count; i++)
            {

                addressDto.CorrespondenceFlatNumber = excelTable.Rows[i].ItemArray[12].ToString();
                addressDto.CorrespondencePostOfficeCity = excelTable.Rows[i].ItemArray[14].ToString();
                addressDto.CorrespondenceStreetName = excelTable.Rows[i].ItemArray[10].ToString();
                addressDto.CorrespondenceStreetnumber = excelTable.Rows[i].ItemArray[11].ToString();
                addressDto.FlatNumber = excelTable.Rows[i].ItemArray[7].ToString();
                addressDto.PostCode = excelTable.Rows[i].ItemArray[8].ToString();
                addressDto.PostOfficeCity = excelTable.Rows[i].ItemArray[9].ToString();
                addressDto.StreetName = excelTable.Rows[i].ItemArray[5].ToString();
                addressDto.StreetNumber = excelTable.Rows[i].ItemArray[6].ToString();
                addressDto.CorrespondencePostCode = excelTable.Rows[i].ItemArray[13].ToString();


                repo.AddAddress(addressDto);
            }


            for (int i = 1; i < excelTable.Rows.Count; i++)
            {

                personDto.FirsName = excelTable.Rows[i].ItemArray[2].ToString();
                personDto.Surname = excelTable.Rows[i].ItemArray[3].ToString();
                personDto.NationalIdentificationNumber = excelTable.Rows[i].ItemArray[4].ToString();
                personDto.Phonenumber = excelTable.Rows[i].ItemArray[15].ToString();
                personDto.Phonenumber2 = excelTable.Rows[i].ItemArray[16].ToString();
                personDto.Id = 10;
                repo.AddPerson(personDto, excelTable.Rows[i].ItemArray[8].ToString(),
                    excelTable.Rows[i].ItemArray[6].ToString(), excelTable.Rows[i].ItemArray[5].ToString());
                
            }

            for (int i = 1; i < excelTable.Rows.Count; i++)
            {
                

                financialStateDto.CourtFees = Parser(excelTable.Rows[i].ItemArray[17].ToString());

                financialStateDto.Fees = Parser(excelTable.Rows[i].ItemArray[18].ToString());
                financialStateDto.Interests = Parser(excelTable.Rows[i].ItemArray[19].ToString());
                financialStateDto.OutstandingLiabilites = Parser(excelTable.Rows[i].ItemArray[20].ToString());
                financialStateDto.PenaltyInterests = Parser(excelTable.Rows[i].ItemArray[21].ToString());
                financialStateDto.RepresentationCourtFees = Parser(excelTable.Rows[i].ItemArray[22].ToString());
                financialStateDto.RepresentationVindicationCosts = Parser(excelTable.Rows[i].ItemArray[24].ToString());
                financialStateDto.VindicationCosts = Parser(excelTable.Rows[i].ItemArray[23].ToString());

                agreementDto.Number = excelTable.Rows[i].ItemArray[1].ToString();

                repo.AddAgreement(agreementDto, excelTable.Rows[i].ItemArray[4].ToString(),financialStateDto);

            }
            
            var person = new ServiceReference1.Person();
            


            return View();

        }

        public static decimal Parser(string toParse)
        {
            decimal number;

            bool result = decimal.TryParse(toParse, out number);
            if (result)
            {
                return number;
            }
            else
            {
                return 0;
            }
        }
    }
}

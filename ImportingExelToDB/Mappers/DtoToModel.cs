﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ImportingExelToDB.Dto;
using ImportingExelToDB.Models;

namespace ImportingExelToDB.Mappers
{
    public class DtoToModel
    {
        public static Person PersonDtoToEntityModel(PersonDto personDto)
        {
            if (personDto == null)
            {
                return null;
            }

            var person = new Person();

            person.FirsName = personDto.FirsName;
            person.NationalIdentificationNumber = personDto.NationalIdentificationNumber;
            person.Addresid = personDto.Addresid;
            person.Phonenumber = personDto.Phonenumber;
            person.Phonenumber2 = personDto.Phonenumber2;
            person.SecondName = personDto.SecondName;
            person.Surname = personDto.Surname;

            return person;
        }

        public static Address AddresDtoToEntityModel(AddressDto addressDto)
        {
            if (addressDto == null)
            {
                return null;
            }

            var address = new Address();
            address.CorrespondenceFlatNumber = addressDto.CorrespondenceFlatNumber;
            address.CorrespondencePostCode = addressDto.CorrespondencePostCode;
            address.CorrespondencePostOfficeCity = addressDto.CorrespondencePostOfficeCity;
            address.CorrespondenceStreetName = addressDto.CorrespondenceStreetName;
            address.CorrespondenceStreetnumber = addressDto.CorrespondenceStreetnumber;
            address.FlatNumber = addressDto.FlatNumber;
            address.PostCode = addressDto.PostCode;
            address.PostOfficeCity = addressDto.PostOfficeCity;
            address.StreetName = addressDto.StreetName;
            address.StreetNumber = addressDto.StreetNumber;


            return address;
        }

        public static Agreement AgreementDtoToEntityModel(AgreementDto aggrementDto)
        {
            if (aggrementDto == null)
            {
                return null;
            }

            var aggrement = new Agreement();
            aggrement.FinancialStateId = aggrementDto.FinancialStateId;
            aggrement.Number = aggrementDto.Number;
            aggrement.Personid = aggrementDto.Personid;

            return aggrement;
        }


        public static FinancialState finecialDtoToEntityModel(FinancialStateDto financialDto)
        {
            if (financialDto == null)
            {
                return null;
            }

            var financial = new FinancialState();
            financial.CourtFees = financialDto.CourtFees;
            financial.Fees = financialDto.Fees;
            financial.Interests = financialDto.Interests;
            financial.OutstandingLiabilites = financialDto.OutstandingLiabilites;
            financial.PenaltyInterests = financialDto.PenaltyInterests;
            financial.RepresentationCourtFees = financialDto.RepresentationCourtFees;
            financial.RepresentationVindicationCosts = financialDto.RepresentationVindicationCosts;
            financial.VindicationCosts = financialDto.VindicationCosts;


            return financial;
        }
    }
}
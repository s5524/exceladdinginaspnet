﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ImportingExelToDB.Dto;
using ImportingExelToDB.ServiceReference1;

namespace ImportingExelToDB.Mappers
{
    public class PersonDtoToReferencePerson
    {

        public static Person personDtoToReferenceService(PersonDto personDto)
        {
            var person = new Person();
            person.Addresses = addressDtoToReferenceService(personDto);
            person.Phones = phoneDtoToReferenceService(personDto);
            person.FinancialState = financialStateDto(personDto);

            return person;
        }

        private static FinancialState financialStateDto(PersonDto personDto)
        {
            var financialState = new FinancialState();
            return financialState;
        }

        public static Address[] addressDtoToReferenceService(PersonDto addressDto)
        {
            Address[] addresses = null;
            var address = new Address();
            address.City = addressDto.Addresid.PostOfficeCity;
            address.HouseNo = addressDto.Addresid.StreetNumber;
            address.LocaleNo = addressDto.Addresid.FlatNumber;
            addresses[0] = address;
            return addresses;

        }
        public static Phone[] phoneDtoToReferenceService(PersonDto personDto)
        {
            Phone[] phones = null;
            
       
            return phones;

        }

    }
}
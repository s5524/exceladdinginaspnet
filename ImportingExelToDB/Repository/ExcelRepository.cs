﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using ImportingExelToDB.DbContexts;
using ImportingExelToDB.Dto;
using ImportingExelToDB.Mappers;
using ImportingExelToDB.Models;

namespace ImportingExelToDB.Repository
{
    public class ExcelRepository
    {


        public void AddAddress(AddressDto addresDto)
        {
            using (var dbContext = new ImportingDbContext())
            {
                dbContext.AddresDbSet.Add(DtoToModel.AddresDtoToEntityModel(addresDto));
                dbContext.SaveChanges();
            }
        }



        public void AddPerson(PersonDto personDto,string postCode,string streetNumber,string streetName)
        {
            using (var dbContext = new ImportingDbContext())
            {
                var addres = dbContext.AddresDbSet.FirstOrDefault(a => a.PostCode == postCode && a.StreetNumber == streetNumber && a.StreetName == streetName);
                personDto.Addresid = addres;
                dbContext.AddresDbSet.Attach(addres);
                dbContext.PersonDbSet.Add(DtoToModel.PersonDtoToEntityModel(personDto));
                dbContext.SaveChanges();
            }
        }

        public void AddFinancialState(FinancialStateDto financialStateDto)
        {
            using (var dbContext = new ImportingDbContext())
            {
                
                dbContext.FinancialDbSet.Add(DtoToModel.finecialDtoToEntityModel(financialStateDto));
                dbContext.SaveChanges();
            }
        }

        public void AddAgreement(AgreementDto AggreementDto,string personPesel,FinancialStateDto financialState)
        {
            using (var dbContext = new ImportingDbContext())
            {
                var person = dbContext.PersonDbSet.FirstOrDefault(a => a.NationalIdentificationNumber == personPesel);
                AggreementDto.FinancialStateId = DtoToModel.finecialDtoToEntityModel(financialState);
                AggreementDto.Personid = person;

                dbContext.PersonDbSet.Attach(person);
                dbContext.FinancialDbSet.Attach(DtoToModel.finecialDtoToEntityModel(financialState));
                dbContext.AgreementDbSet.Add(DtoToModel.AgreementDtoToEntityModel(AggreementDto));
                dbContext.SaveChanges();
            }
        }
    }
}
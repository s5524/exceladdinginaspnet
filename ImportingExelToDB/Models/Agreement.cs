﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ImportingExelToDB.Models
{
    public class Agreement
    {
        public int Id { get; set; }
        public string Number { get; set; }
        public Person Personid { get; set; }
        public FinancialState FinancialStateId { get; set; }
    }
}
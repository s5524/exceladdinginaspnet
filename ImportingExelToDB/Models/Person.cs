﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;


namespace ImportingExelToDB.Models
{
    public class Person
    {
        public int Id { get; set; }
        public string FirsName { get; set; }
        public string SecondName { get; set; }
        public string Surname { get; set; }
        [Required]
        [StringLength(11)]
        public string NationalIdentificationNumber { get; set; }
        public Address Addresid { get; set; }
        public string Phonenumber { get; set; }
        public string Phonenumber2 { get; set; }
    }
}